const checkMatch = (state) => {
    const updatedShuffles = state.shuffledNums.map((number, index) => {
        if (
          state.revealed.length === 2 &&
          (index === state.revealed[0].id || index === state.revealed[1].id)
        ) {
          return {
            ...number,
            isVisible: false
          };
        }
        return number;
      });

      const matchedShuffles = state.shuffledNums.map((number, index) => {
        if (
          state.revealed.length === 2 &&
          (index === state.revealed[0].id || index === state.revealed[1].id)
        ) {
          return {
            ...number,
            isMatch: true
          };
        }
        return number;
      });

      if (state.revealed.length === 2) {
        if (
          state.shuffledNums[state.revealed[0].id].value !==
          state.shuffledNums[state.revealed[1].id].value
        ) {
          return {
            ...state,
            shuffledNums: updatedShuffles,
            revealed: []
          };
        } else {
          return {
            ...state,
            shuffleNums: matchedShuffles,
            revealed: [],
            score: state.score + 1
          };
        }
      }
      return state;
}

export default checkMatch;