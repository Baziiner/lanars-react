const revealOne = (state, action) => {
    const show = {
        id: state.shuffledNums.indexOf(state.shuffledNums[action.id])
      };

      return {
        ...state,
        shuffledNums: state.shuffledNums.map((content, i) =>
          i === action.id ? { ...content, isVisible: true } : content
        ),
        revealed: [...state.revealed, show]
      };
}

export default revealOne;