import shuffle from './shuffle';

const duplicate = (state) => {
	const duplicateNums = [...state.nums, ...state.nums];
	shuffle(duplicateNums);
	return {
			...state,
			shuffledNums: duplicateNums
	};
}

export default duplicate;