const generate = (state, action) => {
    let nums = [];
    let generateTemplate = [];

    const generateNums = () => {
        for (let i = 2; i <= 50; i++) {
            nums.push(i);
        }
    }

    const splicePrime = () => {
        const checkPrime = (value) => {
            for(var i = 2; i < value; i++) {
                if(value % i === 0) {
                    return false;
                }
            }
            return value > 1;
        }

        for(let i = 0; i < nums.length; i++) {
            if(!checkPrime(nums[i])) {
                const index = nums.indexOf(nums[i]);
                if (index > -1) nums.splice(index, 1);
                i--;
            }
        }
    }

    const modifyArray = () => {
    for (let i = 0; i < nums.length; i++) {
        generateTemplate.push({
        value: nums[i],
        isVisible: true,
        isMatch: false
        });
    }
    };

    generateNums();
    splicePrime();
    modifyArray();

    //The loop here had no use. my mistake, as i left it by accident during a refactory (:
    return {
    ...state,
    nums: [...state.nums, generateTemplate[action.payload]]
    };
}

export default generate;