const hide = (state) => {
    return {
        ...state,
        shuffledNums: state.shuffledNums.map(item => ({
          value: item.value,
          isMatch: item.isMatch,
          isVisible: false
        }))
      };
}

export default hide;