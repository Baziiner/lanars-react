import generate from './generate';
import duplicate from './duplicate';
import hide from './hide';
import revealOne from './revealOne';
import checkMatch from './checkMatch';

export { generate, duplicate, hide, revealOne, checkMatch };