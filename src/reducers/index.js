import numberReducer from "./number";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  number: numberReducer
});

export default rootReducer;
