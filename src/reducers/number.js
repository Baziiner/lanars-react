import { 
  generate, 
  duplicate, 
  hide, 
  revealOne, 
  checkMatch
} from "../utils/helpers";

const initialState = {
  nums: [],
  shuffledNums: [],
  revealed: [],
  score: 0
};

const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case "GENERATE":
      return generate(state, action);
    case "DUPLICATE":
      return duplicate(state);
    case "HIDE":
      return hide(state);
    case "REVEALONE":
      return revealOne(state,action);
    case "CHECKMATCH":
      return checkMatch(state);
    case "RESET":
      return {
        nums: [],
        shuffledNums: [],
        revealed: [],
        score: 0
      }
    default:
      return state;
  }
};

export default numberReducer;
