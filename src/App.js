import React from "react";
import { HelmetProvider } from "react-helmet-async";
import { Switch, Route } from "react-router-dom";
import ROUTES from "./view/routes";
import Mahjong from "./view/Mahjong";
import Home from "./view/Home";
import "./App.css";

function App() {
  return (
    <HelmetProvider>
      <Switch>
        <Route exact path={ROUTES.HOME} component={Home} />
        <Mahjong exact path={ROUTES.MAHJONG} component={Mahjong} />
      </Switch>
    </HelmetProvider>
  );
}

export default App;
