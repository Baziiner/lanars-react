export const generate = (count) => {
  return {
    type: "GENERATE",
    payload: count,
  };
};

export const duplicate = () => {
  return {
    type: "DUPLICATE"
  };
};

export const reveal = () => {
  return {
    type: "REVEAL"
  };
};

export const hide = () => {
  return {
    type: "HIDE",
    payload: true
  };
};

export const revealOne = index => {
  return {
    type: "REVEALONE",
    id: index
  };
};

export const checkMatch = () => {
  return {
    type: "CHECKMATCH",
  }
}

export const reset = () => {
  return {
    type: 'RESET'
  }
}
