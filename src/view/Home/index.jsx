import React from "react";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import ROUTES from "../routes";
import "./Home.scss";

const Home = () => {

  return (
    <>
      <Helmet>
        <title>Lanars - Home</title>
      </Helmet>
      <div className="Home">
        <img
          src="https://lanars.com/static/images/lanars_logo_full_size.png"
          alt="Lanars Logo"
          title="Lanars Logo"
          className="mainLogo mt-8"
        />

        <Link to={ROUTES.MOHJANG} className="SelectButton">
          <button className="pl-3">
            Play <span>&#62;</span>
          </button>
        </Link>
      </div>
    </>
  );
};

export default Home;
