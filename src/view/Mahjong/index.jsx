import React, { useEffect } from "react";
import { Helmet } from "react-helmet-async";
import Card from "../../components/Card";
import { useSelector, useDispatch } from "react-redux";
import {
  generate,
  duplicate,
  hide,
  revealOne,
  checkMatch,
  reset
} from "../../actions";
import "./Mohjang.scss";

const Mahjong = () => {
  const dispatch = useDispatch();
  const shuffledNums = useSelector(state => state.number.shuffledNums);
  const check = useSelector(state => state.number.revealed);
  const score = useSelector(state => state.number.score);

  useEffect(() => {
    dispatch(checkMatch());
  }, [check, dispatch]);

  useEffect(() => {
    for (let i = 0; i < 15; i++) {
      dispatch(generate(i));
    }
    dispatch(duplicate());
    const hideAll = setTimeout(() => {
      dispatch(hide());
    }, 1000);

    return () => {
      clearTimeout(hideAll);
      dispatch(reset());
    };
  }, [dispatch]);

  const handleClick = index => {
    dispatch(revealOne(index));
  };

  return (
    <>
      <Helmet>
        <title>Lanars - Mohjang-like game</title>
      </Helmet>
      <div className="Mohjang p-2">
        <h1 className="title">Mahjong-like game</h1>
        <h2 className="Score-meter">Your Score <span className={score === 15 ? 'success' : ''}>{score}/15</span></h2>
        <div className="Mohjang-table">
          {shuffledNums.map((item, index) => (
            <Card
              key={index}
              index={index}
              val={item.value}
              isVisible={item.isVisible}
              isMatch={item.isMatch || ""}
              handleClick={handleClick}
            />
          ))}
        </div>
      </div>
    </>
  );
};

export default Mahjong;
