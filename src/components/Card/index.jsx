import React from "react";
import "./Card.scss";

const Card = ({ index, val, isVisible, isMatch, handleClick }) => (
  <button
    className={"Card" + (isVisible || isMatch ? '' : ' -hidden ') + (isMatch && isVisible ? ' -success' : '')}
    index={index + 1}
    onClick={() => {
      handleClick(index);
    }}
    disabled={isVisible || isMatch}
  >
   <span>{val}</span> 
  </button>
);

export default Card;
